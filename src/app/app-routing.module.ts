import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path:'login', component:LoginComponent },
  { path: 'welcome', component: HomeComponent}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ enableTracing:false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
