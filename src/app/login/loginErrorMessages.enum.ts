export enum LoginErrorMessages{
    UserNameReq = 'User Name is required.',
    PasswordReq = 'Password is required.',
    BothReq = 'User Name and Password is required.',
    Success = 'User logged in successfully.'
}