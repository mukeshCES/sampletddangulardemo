import { TestBed, ComponentFixture } from "@angular/core/testing";
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './auth.service';
// import { RouterTestingModule } from '@angular/router/testing';
import { LoginErrorMessages } from './loginErrorMessages.enum';
import {  } from 'protractor';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';



describe('Login Component', () => {
    let mockAuthService: jasmine.SpyObj<AuthenticationService>;
    let fixture:ComponentFixture<LoginComponent>;
    let mockrouter;
    

    beforeEach(() => {
        mockAuthService = jasmine.createSpyObj(['login']);
        
        mockrouter = jasmine.createSpyObj(['navigate','navigateByUrl']);

        TestBed.configureTestingModule({
            declarations:[LoginComponent],
            imports: [  ReactiveFormsModule, 
                        // RouterTestingModule
                ],
            providers:[
                { provide: AuthenticationService, useValue: mockAuthService },
                { provide: Router, useValue: mockrouter},
            ]
        })

        fixture = TestBed.createComponent(LoginComponent);
    });



    it('should have the Login button disabled initially', () =>{
        //arrange
        
        //act
        fixture.detectChanges();

        //assert        
        expect(fixture.debugElement.query(By.css('#submit')).nativeElement.disabled).toBe(true);
    });

    it('should have the login button disabled when the login form is invalid (all the validation for any of the fields is invalid)',() => {
        //arrange 

        //act
        fixture.detectChanges();

        //assert
        // expect(fixture.componentInstance.loginForm.controls.Name.errors).toBe(!null);
        expect(fixture.componentInstance.loginForm.valid).toBe(false);
        expect(fixture.debugElement.query(By.css('#submit')).nativeElement.disabled).toBe(true);
    });

    it('should enable the login button when login form is valid.',() => {
        //arrange 
        fixture.detectChanges();

        //act
        fixture.componentInstance.loginForm.patchValue({
            Name:'xyz',
            Password:'xyz'
        });
        fixture.detectChanges();

        //assert
        expect(fixture.componentInstance.loginForm.valid).toBe(true);
        expect(fixture.debugElement.query(By.css('#submit')).nativeElement.disabled).toBe(false);
    });

    
        it('should show the error message for both user name and password field when both are empty',() => {
            //arrange 
            fixture.detectChanges();

            //act
            fixture.componentInstance.loginForm.patchValue({
                Name:'',
                Password:''
            });
            fixture.detectChanges();

            //assert
            // expect(fixture.componentInstance.loginForm.controls.Name.errors).toBe(!null);
            expect(fixture.componentInstance.displayMessage.Name).toBe(LoginErrorMessages.UserNameReq);
            expect(fixture.componentInstance.displayMessage.Password).toBe(LoginErrorMessages.PasswordReq);
        });
    

        it('should set the error message to "User name is required". when user name is not passed',() => {
            //arrange 
            fixture.detectChanges();

            //act
            fixture.componentInstance.loginForm.patchValue({
                Name:'',
                Password:'password'
            });
            fixture.detectChanges();

            //assert
            // expect(fixture.componentInstance.loginForm.controls.Name.errors).toBe(!null);
            expect(fixture.componentInstance.displayMessage.Name).toBe(LoginErrorMessages.UserNameReq);
            
        });

        it('should not show "User name is required" error message when user name is  passed',() => {
            //arrange 
            fixture.detectChanges();

            //act
            fixture.componentInstance.loginForm.patchValue({
                Name:'xyz',
                Password:''
            });
            fixture.detectChanges();

            //assert
            // expect(fixture.componentInstance.loginForm.controls.Name.errors).toBe(!null);
            expect(fixture.componentInstance.displayMessage.Name).toBe('');
            
        });
        
        it('should set the error message to "Password is required". when password is not passed',() => {
            //arrange
            fixture.detectChanges(); //runs ngOnInit 

            //act
            fixture.componentInstance.loginForm.patchValue({
                Name:'mukesh',
                Password:''
            });
            fixture.detectChanges();

            //assert
            // expect(fixture.componentInstance.loginForm.controls.Password.errors).toBe(!null);
            expect(fixture.componentInstance.displayMessage.Password).toBe(LoginErrorMessages.PasswordReq);
            


        });

        it('should not show "Password is required" error message when password is passed',() => {
            //arrange
            fixture.detectChanges(); //runs ngOnInit 

            //act
            fixture.componentInstance.loginForm.patchValue({
                Name:'',
                Password:'xyz'
            });
            fixture.detectChanges();

            //assert
            // expect(fixture.componentInstance.loginForm.controls.Password.errors).toBe(!null);
            expect(fixture.componentInstance.displayMessage.Password).toBe('');
        });

        describe('login method',() => {
            

            it('should login the user',() => {
                //arrange 
                mockAuthService.login.and.returnValue(LoginErrorMessages.Success)
                let username = 'xyz';
                fixture.detectChanges();
                fixture.componentInstance.loginForm.patchValue({
                    Name:username,
                    Password:'xyz'
                });
                fixture.detectChanges();

                //act
                fixture.debugElement.query(By.css('#submit')).nativeElement.click();

                //assert
                expect(mockAuthService.login).toHaveBeenCalledWith(fixture.componentInstance.loginForm.value);
            });

            it('should redirect to home page when login is successful and redirect url is not present',() =>{
                //arrange
                mockAuthService.login.and.returnValue(LoginErrorMessages.Success)
                let username = 'xyz';
                fixture.detectChanges();
                fixture.componentInstance.loginForm.patchValue({
                    Name:username,
                    Password:'xyz'
                });
                fixture.detectChanges();

                //act
                fixture.debugElement.query(By.css('#submit')).nativeElement.click();

                //assert
                expect(mockrouter.navigate).toHaveBeenCalledWith(['/welcome']);

            });

            
        });
    
});

//this is to test the redirect part when there is a redirect url in the authentication service.
describe('Login Component redirect on login', ()=>{


    it('should log in the user and redirect to the specified path in the redirect url', () => {
        //arrange
        let mockAuthService: jasmine.SpyObj<AuthenticationService>;
        let fixture:ComponentFixture<LoginComponent>;
        let mockrouter;
        mockAuthService = {
            ...jasmine.createSpyObj('mockAuthService',['login']),
            redirectUrl:'/dummyRoute'
        } as jasmine.SpyObj<AuthenticationService>;
        mockrouter = jasmine.createSpyObj(['navigate','navigateByUrl']);
    
        TestBed.configureTestingModule({
            declarations:[LoginComponent],
            imports: [  ReactiveFormsModule, 
                // RouterTestingModule
            ],
            providers:[
                { provide: AuthenticationService, useValue: mockAuthService },
                { provide: Router, useValue: mockrouter},
            ]
        });
    
        fixture = TestBed.createComponent(LoginComponent);
        mockAuthService.login.and.returnValue(LoginErrorMessages.Success);
        mockAuthService.redirectUrl = '/dummyRoute';
        let username = 'xyz';
        fixture.detectChanges();
        fixture.componentInstance.loginForm.patchValue({
            Name:username,
            Password:'xyz'
        });
        fixture.detectChanges();
    
        //act
        fixture.debugElement.query(By.css('#submit')).nativeElement.click();
    
        //assert
        expect(mockrouter.navigateByUrl).toHaveBeenCalledWith('/dummyRoute');
        
    });
})

