import { AuthGuard } from "./auth.guard";
import { RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

describe('Auth Guard', () => {
    
    describe('canActivate method', () => {

        let mockAuthService;
        let mockRouter;
        let authGuard:AuthGuard;
        let route: jasmine.SpyObj<ActivatedRouteSnapshot>;
        let state: jasmine.SpyObj<RouterStateSnapshot>;

        beforeEach( () => {
            mockAuthService = jasmine.createSpyObj(['getUser']);
            mockRouter = jasmine.createSpyObj(['navigate']);

            authGuard = new AuthGuard(mockAuthService, mockRouter);
        });

        it('should redirect to login page when user is not logged in',() => {
            //arrange 
            mockAuthService.getUser.and.returnValue(null);
            state = {
                ...state,
                url:'/products'
            } as jasmine.SpyObj<RouterStateSnapshot>
            
            //act
            let result = authGuard.canActivate(route,state);

            //assert
            expect(mockRouter.navigate).toHaveBeenCalledWith(['/login']);
            expect(result).toBe(false);
        });

        it('should return true if user is logged in', () => {

            //arrange
            mockAuthService.getUser.and.returnValue({Name:'xyz',Password:'xyz'});

            //act

            //assertt
            expect(authGuard.canActivate(route,state)).toBe(true);

        });
    
    });
})