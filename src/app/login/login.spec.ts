import { AuthenticationService } from "./auth.service";
import { User } from './user';
import { LoginErrorMessages } from './loginErrorMessages.enum';

describe('Authentication Service ', () => {

    describe('Get User Method',() => {
        let authService : AuthenticationService;
        beforeEach(() => {
            authService = new AuthenticationService();
        })
        it('should return null if there is no logged in user',()=>{
            //arrange
            
    
            //act
            
    
            //assert
            expect(authService.getUser()).toBe(null);
    
        });
    
        it('should return the logged in user if there is a user logged in.',()=>{
            //arrange
            let user:User = { Name:'mukesh', Password:'password'};
    
            //act
            authService.login(user);
    
            //assert
            expect(authService.getUser()).toBe(user);
    
        });
    });

    

    describe('on Login', ()=>{
        let authService:AuthenticationService;
        beforeEach(() => {
            authService = new AuthenticationService();
        })
        it('should say "User Name is required." when user name is not passed.', () =>{
            //arrange
            let user:User = {Name:'',Password:''};
    
            //act
            let result = authService.login(user);
    
            //assert
            expect(result).toBe(LoginErrorMessages.UserNameReq);
        });

        it('should say "Password is required." when password is not sent.', () => {
            //arrange
            let user:User = {Name:'mukesh', Password:''};

            //act
            let result = authService.login(user);

            //assert
            expect(result).toBe(LoginErrorMessages.PasswordReq);
        });

        it('should say "user name and password is required" when both are not passed.', () =>{
            //arrange
            let user:User = null;

            //act
            let result = authService.login(user);

            //assert
            expect(result).toBe(LoginErrorMessages.BothReq)
        });

        it('should say "user logged in successfully" when user name and password are passed.', () =>{
            //arrange
            let user:User = { Name:'mukesh', Password:'password'};

            //act
            let result = authService.login(user);

            //assert
            expect(result).toBe(LoginErrorMessages.Success)
        });
    });

    describe('logoff method', ()=>{
        
        it('should log off the current user and logged in user value should be null',()=>{
            //arrange
            let authService = new AuthenticationService();
            let user:User = { Name: 'Mukesh', Password: 'xyz'};

            //act
            authService.login(user);
            authService.logOff();

            //asser
            expect(authService.getUser()).toBe(null);
        });

        xit('should log off the current user and invoking to get user should return null',() => {


        });
    });
});

