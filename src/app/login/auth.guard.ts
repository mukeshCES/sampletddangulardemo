import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate{

    constructor(private authService: AuthenticationService, private router: Router ){

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        //load product path if logged in
        if(this.authService.getUser()){
            return true;
        }
        //else navigate to login page
        this.authService.redirectUrl = state.url;
        this.router.navigate(['/login']);
        return false;
    }
}