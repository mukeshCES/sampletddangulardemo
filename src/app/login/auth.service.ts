import { Injectable } from "@angular/core";
import { User } from './user';
import { LoginErrorMessages } from './loginErrorMessages.enum';

@Injectable()
export class AuthenticationService{
    private loggedInUser: User = null;
    redirectUrl :string = null;
    
    
    login(user:User):string{
        if(!user){
            return LoginErrorMessages.BothReq; //User Name and Password is required.
        }
        if(!user.Name){
            return LoginErrorMessages.UserNameReq; //User Name is required.
        }
        if(!user.Password){
            return LoginErrorMessages.PasswordReq; //Password is required.
        }

        this.loggedInUser = user;
        console.log("Logged in User:", this.loggedInUser);
        return LoginErrorMessages.Success; //User logged in successfully.
        
    }
    getUser():User{
        return this.loggedInUser;
    }

    logOff(){
        this.loggedInUser = null;
    }
}