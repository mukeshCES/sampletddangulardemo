import { Component, OnInit, AfterViewInit } from "@angular/core";
import { AuthenticationService } from './auth.service';
import { User } from './user';
import { Router } from '@angular/router';
import { LoginErrorMessages } from './loginErrorMessages.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericValidator } from '../shared/generic-validator';

@Component({
    templateUrl:'./login.component.html'
})
export class LoginComponent implements OnInit, AfterViewInit {
    
    title:string = 'Login Component';
    loginForm:FormGroup;
    errorMessage:string ;

    validationMessages:{ [key: string] : { [key: string]: string } }
    displayMessage:{ [key: string]: string } = {};

    private genericValidator :GenericValidator;

    constructor( private authService:AuthenticationService, 
                private route: Router, 
                private fb: FormBuilder 
                ){

    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            Name: ['', Validators.required],
            Password: ['', Validators.required]
        });

        this.initializeValidationMessages();

        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngAfterViewInit(): void {
        this.loginForm.valueChanges.subscribe(value => {
            this.displayMessage = this.genericValidator.processValidations(this.loginForm);
        })
    }

    

    login(){
        if(this.loginForm.valid){
            let loginResult = this.authService.login(this.loginForm.value);

            if(loginResult == LoginErrorMessages.Success){
                if(this.authService.redirectUrl)
                    this.route.navigateByUrl(this.authService.redirectUrl);
                else
                    this.route.navigate(['/welcome']);
            }
            else{
                this.setErrorMessage(loginResult)
            }
        }
    }

    setErrorMessage(message:string){
        this.errorMessage = message;
    }

    initializeValidationMessages():void{

        this.validationMessages = {
            Name:{
                required: LoginErrorMessages.UserNameReq
            },
            Password:{
                required: LoginErrorMessages.PasswordReq
            }
        };

        
    }

}