import { FormGroup } from '@angular/forms';

export class GenericValidator{


    constructor(private validationMessages: { [ key: string ] : { [ key:string ] : { } } }){

    }

    processValidations(container: FormGroup): { [ key: string ]: string }{
        let messages = {};
        
        for(const controlKey in container.controls){
            if(container.controls.hasOwnProperty(controlKey)){ 
                const control = container.controls[controlKey];

                if(this.validationMessages[controlKey]){
                    messages[controlKey] = '';
                    if(control.errors){
                        Object.keys(control.errors).map( messageKey => {
                            if(this.validationMessages[controlKey][messageKey]){
                                messages[controlKey] += this.validationMessages[controlKey][messageKey];
                            }
                        });
                    }
                }
            }
        }

        return messages;
    }
}